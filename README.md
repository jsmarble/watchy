# Watchy #

A simple stopwatch application supporting splits, laps, starting, and stopping. Printing and saving results is also supported.

Copyright 2016